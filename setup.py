#!/usr/bin/env python
# -*- coding: utf-8 -*-
try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


def parse_requirements(filename):
    """ load requirements from a pip requirements file """
    lineiter = (line.strip() for line in open(filename))
    return [line for line in lineiter if line and not line.startswith("#")]


with open('README.md') as readme_file:
    readme = readme_file.read()

with open('CHANGELOG.md') as changelog_file:
    changelog = changelog_file.read()


requirements = parse_requirements('requirements.txt')
test_requirements = parse_requirements('testing_requirements.txt')

setup(
    name='python_roshambo_runner',
    version='0.1.0',
    description="An Engine that rupits RoShamBo bots against each other",
    long_description=readme + '\n\n' + changelog,
    author="Jeffery Tillotson",
    author_email='jpt@jeffx.com',
    url='https://github.com/jeffx/python_roshambo_runner',
    packages=[
        'python_roshambo_runner',
        'bots'
    ],
    package_dir={'python_roshambo_runner':
                 'python_roshambo_runner'},
    include_package_data=True,
    install_requires=requirements,
    license="GPLv3",
    zip_safe=False,
    keywords='python_roshambo_runner',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.4',
    ],
    test_suite='tests',
    tests_require=test_requirements,
    entry_points={
        'console_scripts': [
            'roshambo = python_roshambo_runner.cli:cli'
        ]
    }
)
