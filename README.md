# Python RoShamBo Runner
---

An Engine that pits RoShamBo bots against each other


Dependencies
------------

 * prettytable


Usage
-----

 * `pip install git+https://github.com/jeffx/python_roshambo_runner.git@master#egg=python_roshambo_runner`
 * Place bots in the bot directory
 * run `roshambo`


License
-------

GNU General Public License v3 (GPLv3)

Contributors
------------

 - Jeffery Tillotson <jeffx@jeffx.com>

## Tournament
### Rules

*Summary:*  Write a program to play Rock-Paper-Scissors, and enter it to play in an open competition against other programs. 

*Purpose:* To learn Python and have a little fun 

1. All bots must be written in Python 3 using the [Python Standard Library](https://docs.python.org/3/library/index.html)
2. Bots that throw an exception will be scored as DNQ
3. Bots must not print messages to stdout
4. Each bot plays a match against all other bots in the tournament
5. Each bot will be a function that takes two parameters and returns a single integer value of 0, 1, or 2:
    * 0 = rock
	* 1 = paper
	* 2 = scissors

   ```python
   def bot_entry_function(opponent=None, mine=None):
   ```

4. The length of each match is 1000 rounds
5. A numerical result for number of rounds won in each match is calculated and used for scoring.  See scoring
6. There is no rule 8
7. Any attempt to thwart the tournament, the arena, or the host computer will result in disqualification
8. The tournament organizers will not modify your bot in anyway

### Scoring
1. A match is scored based on the amount of winning rounds
2. A match is scored: 2 points are awarded for a win.  1 point for a draw
3. Tie-breakers are in the following order:
	1. Amount of total hand wins for the tournament *Note: the scoreboard does not sort on this.  See Issue #6*

### To Enter
1. Write a bot, in a single file with the .py file extension:
   * Must contain the following module level variables
   ```python
   __botname__ = "Insert Bot Name Here"
   __entryfunction__ = "entry_function"  # Must match the name of your entry function in item b
   __developer__ = "Your Name"
   ```

   * Entry function must accept two lists.  The first is your opponents match history.  The second is your match history

   ```python
   def entry_function(opponent=None, mine=None):
       return 0
   ```
2. Give your bot to the tournament organizer at least an hour before match time.  There is no formal entry procedure, but be aware of copy and past concerns .

### Questions Needing Answers
1. Should bots be made open after the tournament?


### Credits
This tournament, the rules, the whole damn thing borrowed heavily from *The International Roshambo Contest* ran out of the University of Alberta.  Details can be found [Here](https://webdocs.cs.ualberta.ca/~darse/rsbpc.html)

The rules are adaped from [Here](https://webdocs.cs.ualberta.ca/~darse/rsbpc2.html)
